#!/bin/bash
# Retrieve the information line of dmesg about the pl2303 connection
string=$(dmesg | grep tty | grep pl2303)
# Regular expression to extract the port where pl2303 is connected
regex="ttyUSB([0-9]|10)$"
echo "HELLO"
# If the string match with the regex
if [[ $string =~ $regex ]]; 
then
	# write in the file port.txt
	cat <<- EOF > port.txt
	${BASH_REMATCH[0]}
	EOF
else
	cat <<- EOF > port.txt
	"Error, the port cannot be read. Regex issue ?"
	EOF
fi
