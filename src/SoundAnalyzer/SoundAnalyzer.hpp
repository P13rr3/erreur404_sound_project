#ifndef ERREUR_404_SOUND_ANALYZER
#define ERREUR_404_SOUND_ANALYZER

// FIXME absolut path...
#include "/home/pierre/Documents/of_v0.10.0_linux64gcc6_release/apps/myApps/erreur404_sound_project/src/Gist/src/Gist.h"

#define NB_OUTPUTS 0
#define NB_INPUTS 1
#define SAMPLE_RATE 44100
#define BUFFER_SIZE 512
#define NB_BUFFER 4

/**
 * @brief This class will be filled with audio data frame. 
 * 				It will analyze the frame and outputs different audio features such as:
 * 				- RMS
 *				- Peak Energy
 *				- Zero Crossing Rate
 *				- Spectral Centroid
 *				- Spectral Crest
 *				- Spectral Flatness
 *				- Spectral Roll Off
 * 				- Spectral Kurtois
 *				- Energy difference
 *				- Spectral difference
 *				- Complex Spectral difference
 *				- High Frequency Content
 *				- FFT Magnitude Spectrum
 *				- Estimated pitch
 *				- Mel-frequency Spectrum
 *				- MFCCs
 *
 * This object use the Gist Library: 
 * @see https://github.com/adamstark/Gist
 */
namespace Erreur404Sound
{
class SoundAnalyzer
{
	public:
		/**
		 * @brief default constructor
		 *
		 */
		SoundAnalyzer();

		/**
		 * @brief This method is an utility methods, to print informations about audio HW
		 *
		 */
		void printAudioInfo();
		
		/** 
		 * @brief Set the audio frame. This audio frame will be analyzed by the object
		 *
		 * @param pData Pointer to the datas
		 * @param bufferSize The buffer size of the datas
		 */
		void setAudioData(float* pData, int bufferSize);
		
		/**
		 * @brief Get the calculated RMS value
		 *
		 * @return The actual RMS value
		 */
		float getRMS();
		
		/**
		 * @brief Get the calculated Peak Energy value
		 *
		 * @return The actual Peak Energy value
		 */
		float getPeakEnergy();
		
		/**
		 * @brief Get the calculated Zero Crossing Rate value
		 *
		 * @return The actual Zero Crossing Rate value
		 */
		float getZeroCrossingRate();
		
		/**
		 * @brief Get the calculated Spectral Centroid value
		 *
		 * @return The actual Spectral Centroid value
		 */
		float getSpectralCentroid();
		 
		/**
		 * @brief Get the calculated Spectral Crest value
		 *
		 * @return The actual Spectral Crest value
		 */
		float getSpectralCrest();
		 
		/**
		 * @brief Get the calculated Spectral Flatness value
		 *
		 * @return The actual Spectral Flatness value
		 */
		float getSpectralFlatness();
		
		/**
		 * @brief Get the calculated Spectral RollOff value
		 *
		 * @return The actual Spectral RollOff value
		 */
		float getSpectralRollOff();
		 
		/**
		 * @brief Get the calculated Spectral Kurtosis value
		 *
		 * @return The actual Spectral Kurtosis value
		 */
		float getSpectralKurtosis();
		 
		/**
		 * @brief Get the calculated Energy Difference value
		 *
		 * @return The actual Energy Difference value
		 */
		float getEnergyDiff();
		 
		/**
		 * @brief Get the calculated Spectral Difference value
		 *
		 * @return The actual Spectral Difference value
		 */
		float getSpectralDiff();
		 
		/**
		 * @brief Get the calculated Spectral Difference value (half wave rectified)
		 *
		 * @return The actual Spectral Difference value (half wave rectified)
		 */
		float getSpectralDiffHwr();
		 
		/**
		 * @brief Get the calculated Complex Spectral Difference value
		 *
		 * @return The actual Complex Spectral Difference value
		 */	 
		float getComplexeSpectralDiff();
		 
		/**
		 * @brief Get the calculated High Frequencey Content value
		 *
		 * @return The actual High Frequency Content value
		 */	 
		float getHFContent();
		 
		/**
		 * @brief Get the calculated FFT Magnitude Spectrum value
		 *
		 * @return The actual FFT Magnitude Spectrum value
		 */	 
		std::vector<float> getMagnitudeSpectrum();
		 
		/**
		 * @brief Get the calculated Pitch value
		 *
		 * @return The actual Picth value
		 */	
		float getPitch();
		
		/**
		 * @brief Get the calculated Mel Frequency Spectrum value
		 *
		 * @return The actual Mel frequency Spectrum value
		 */
		 std::vector<float> getMelFreqSpectrum();
		 
		/**
		 * @brief Get the calculated MFCC value
		 *
		 * @return The actual MFCC value
		 */		
		 std::vector<float> getMFCC();
		 
		
	private:
		Gist<float> gist_m;
		//std::vector<float> soundData_m;
		float audioFrame_m[BUFFER_SIZE];
		
};
}

#endif // ERREUR_404_SOUND_ANALYZER
