#include "SoundAnalyzer.hpp"

namespace Erreur404Sound
{
	SoundAnalyzer::SoundAnalyzer():
		 gist_m(BUFFER_SIZE, SAMPLE_RATE)
	{
	}
	
	void SoundAnalyzer::printAudioInfo()
	{
		//cout << "\n\n----------------------------------------------------------------------" << endl;
		//cout << "RMS:\t" << soundAnalyzer_m->getRMS() << endl;
		//cout << "Peak Energy:\t" << static_cast<uint16_t>(soundAnalyzer_m->getPeakEnergy() *  FACTOR_PEAK_ENERGY)<< endl;
		//cout << "Zero Crossing Rate:\t" << soundAnalyzer_m->getZeroCrossingRate() << endl;
		//cout << "Spectral Centroid:\t" << soundAnalyzer_m->getSpectralCentroid() << endl;
		//cout << "Spectral Crest:\t" << soundAnalyzer_m->getSpectralCrest() << endl;
		//cout << "Spectral Flatness:\t" << soundAnalyzer_m->getSpectralFlatness() << endl;
		//cout << "Spectral RollOff:\t" << soundAnalyzer_m->getSpectralRollOff() << endl;
		//cout << "Spectral Kurtosis:\t" << soundAnalyzer_m->getSpectralKurtosis() << endl;
		//cout << "Energy Difference:\t" << (soundAnalyzer_m->getEnergyDiff() * FACTOR_ENERGY_DIFF) << endl;
		//cout << "Spectral Difference:\t" << soundAnalyzer_m->getSpectralDiff() << endl;
		//cout << "HF content:\t" << soundAnalyzer_m->getHFContent() << endl;
		//cout << "Pitch:\t" << soundAnalyzer_m->getPitch() << endl;
		//cout << "X : " << ofGetMouseX() << "\tY : " << ofGetMouseY() <<endl;
	}
	
	void SoundAnalyzer::setAudioData(float* pData, int bufferSize)
	{
		if(bufferSize <= BUFFER_SIZE)
		{
			gist_m.processAudioFrame(pData, bufferSize);
		}
		else
		{
			// Error, bufferSize is too big
		}
	}
	
	float SoundAnalyzer::getRMS()
	{
		return gist_m.rootMeanSquare();
	}
	
	float SoundAnalyzer::getPeakEnergy()
	{
		return gist_m.peakEnergy();
	}
	
	float SoundAnalyzer::getZeroCrossingRate()
	{
		return gist_m.zeroCrossingRate();
	}
	
	float SoundAnalyzer::getSpectralCentroid()
	{
		return gist_m.spectralCentroid();
	}
	
	float SoundAnalyzer::getSpectralCrest()
	{
		return gist_m.spectralCrest();
	}
	
	float SoundAnalyzer::getSpectralFlatness()
	{
		return gist_m.spectralFlatness();
	}
	
	float SoundAnalyzer::getSpectralRollOff()
	{
		return gist_m.spectralRolloff();
	}

	float SoundAnalyzer::getSpectralKurtosis()
	{
		return gist_m.spectralKurtosis();
	}
	
	float SoundAnalyzer::getEnergyDiff()
	{
		return gist_m.energyDifference();
	}
	
	float SoundAnalyzer::getSpectralDiff()
	{
		return gist_m.spectralDifference();
	}
	
	float SoundAnalyzer::getSpectralDiffHwr()
	{
		return gist_m.spectralDifferenceHWR();
	}
	
	float SoundAnalyzer::getComplexeSpectralDiff()
	{
		return gist_m.complexSpectralDifference();
	}
	
	float SoundAnalyzer::getHFContent()
	{
		return gist_m.highFrequencyContent();
	}
	
	std::vector<float> SoundAnalyzer::getMagnitudeSpectrum()
	{
		return gist_m.getMagnitudeSpectrum();
	}
	
	float SoundAnalyzer::getPitch()
	{
		return gist_m.pitch();
	}
	
	std::vector<float> SoundAnalyzer::getMelFreqSpectrum()
	{
		return gist_m.getMelFrequencySpectrum();
	}
	
	std::vector<float> SoundAnalyzer::getMFCC()
	{
		return gist_m.getMelFrequencyCepstralCoefficients();
	}
}






























