#pragma once

#ifdef __unix__         
#define OS_UNIX
#elif defined(_WIN32) || defined(WIN32) 
#define OS_Windows
#endif

#include "ofMain.h"

#include "ofxGui.h"
#include "SoundAnalyzer/SoundAnalyzer.hpp"

class ofApp : public ofBaseApp{
	public:
		ofApp();
		void setup();
		void update();
		void draw();
		
		void audioIn(float * input, int bufferSize, int nChannels);
		
		/*void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y);
		void mouseDragged(int x, int y, int button);
		void mousePressed(int int, x y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);*/
		
		// Serial communication enum declaration
		enum
		{
			BAUD_RATE = 57600,
			DATA_BYTES_LENGTH = 21,
			START_SIGNAL_LENGTH = 1,
			END_SIGNAL_LENGTH = 1,
			SERIAL_BUFFER_LENGTH = START_SIGNAL_LENGTH + DATA_BYTES_LENGTH + END_SIGNAL_LENGTH,
			START_SIGNAL = 0x10,
			END_SIGNAL = 0xFF,
		};
		
		// Factor for audio data
		enum
		{
			FACTOR_RMS 							 = 1000,
			FACTOR_PEAK_ENERGY 			 = 100,
			FACTOR_SPECTRAL_CENTROID = 10,
			FACTOR_SPECTRAL_CREST 	 = 10,
			FACTOR_SPECTRAL_FLATNESS = 100,
			FACTOR_SPECTRAL_ROLL_OFF = 1000,
			FACTOR_SPECTRAL_KURTOSIS = 100,
			FACTOR_ENERGY_DIFF			 = 10,
		};
		
		enum
		{
		    NB_OF_SOUND_VALUE = 12,
	    };
		
	private:
		/**
		 * @brief Setup the sound objects
		 *
		 */
		void setupSound();
		
		/**
		 * @brief Setup the Serial object
		 *
		 */
		void setupSerialCom();
		
		void sendDataToSerial();
		
		void guiValueUpdate();
	
		void debugTerm();
		/**
		 * @brief This method will display the spectrogram on the app screen
		 *
		 */
		void soundSpectro();
		
		Erreur404Sound::SoundAnalyzer* soundAnalyzer_m;
		Gist<float> gist_m;
		ofMutex soundMutex_m;
		ofSerial serial_m;
		ofSoundStream soundstream_m;
		
		ofxPanel gui_m;
		ofParameter<float> valuesToDisplay_m[NB_OF_SOUND_VALUE]; // The sound analyzer can return 14 values of the sound

};
