#include "ofApp.h"

#include <unistd.h>
#include <stdint.h>

using namespace std;

ofApp::ofApp():
	ofBaseApp(),
	gist_m(BUFFER_SIZE, SAMPLE_RATE)
{
}

//--------------------------------------------------------------
void ofApp::setup()
{
	setupSound();
	
	setupSerialCom();
	
	gui_m.setup();
	gui_m.setPosition(1024 - gui_m.getWidth(), 0);
	gui_m.setName("Sound Analysis");
	for(uint8_t i = 0; i < NB_OF_SOUND_VALUE; i++) gui_m.add(valuesToDisplay_m[i]);
	ofBackground(0);
	usleep(1000 * 1000);
}

//--------------------------------------------------------------
void ofApp::update()
{
	// This method fill the buffer and send it
	sendDataToSerial(); 
	guiValueUpdate();
	usleep(1000 * 30);
}

//--------------------------------------------------------------
void ofApp::draw()
{
	soundSpectro();
	gui_m.draw();
	//cout<<static_cast<uint16_t>(soundAnalyzer_m->getSpectralCentroid())<<endl;
}

//--------------------------------------------------------------
void ofApp::audioIn(float * input, int bufferSize, int nChannels)
{
	usleep(100);
	soundMutex_m.lock();
	soundAnalyzer_m->setAudioData(input, bufferSize);
	soundMutex_m.unlock();
}

/*
 *	RMS:							2 bytes
 * 	Peak Energy: 			1 byte
 *	ZCR : 						2 bytes
 *	Spec. Centroid : 	2 bytes
 *  Spec. Crest : 		2 bytes
 *  Spec. Flatness : 	1 byte
 *	Spec. RollOff : 	1 byte /!\ Big factor, see in real app if there is no overflow
 *  Spec. Kurtosis : 	2 bytes
 *  Energy Diff	:			2 bytes
 *  Spec. Diff : 			2 bytes No factor
 *  HF Content:				2 bytes No factor
 *  Pitch:				2 bytes No factor
 *  Sum ------------- 21 bytes
 *
 */
void ofApp::sendDataToSerial()
{
	char dataBuffer[SERIAL_BUFFER_LENGTH];
	
	// format values
	uint16_t rms = static_cast<uint16_t>(soundAnalyzer_m->getRMS() * FACTOR_RMS);
	uint8_t peakNRJ = static_cast<uint8_t>(soundAnalyzer_m->getPeakEnergy() * FACTOR_PEAK_ENERGY);
	uint16_t zcr = static_cast<uint16_t>(soundAnalyzer_m->getZeroCrossingRate());
	uint16_t specCentroid = static_cast<uint16_t>(soundAnalyzer_m->getSpectralCentroid() * FACTOR_SPECTRAL_CENTROID);
	uint16_t specCrest = static_cast<uint16_t>(soundAnalyzer_m->getSpectralCrest() * FACTOR_SPECTRAL_CREST);
	uint8_t specFlatness = static_cast<uint8_t>(soundAnalyzer_m->getSpectralFlatness() * FACTOR_SPECTRAL_FLATNESS);
	uint8_t rollOff = static_cast<uint8_t>(soundAnalyzer_m->getSpectralRollOff() * FACTOR_SPECTRAL_ROLL_OFF);
	uint16_t specKurtosis = static_cast<uint16_t>(soundAnalyzer_m->getSpectralKurtosis() * FACTOR_SPECTRAL_KURTOSIS);
	uint16_t nrjDiff = static_cast<uint16_t>(soundAnalyzer_m->getEnergyDiff() * FACTOR_ENERGY_DIFF);
	uint16_t specDiff = static_cast<uint16_t>(soundAnalyzer_m->getSpectralDiff());
	uint16_t hfContent = static_cast<uint16_t>(soundAnalyzer_m->getHFContent());
	uint16_t pitch = static_cast<uint16_t>(soundAnalyzer_m->getPitch());
	
	
	// Fill the data buffer
	// Start communication with start signal 1010
	dataBuffer[0] = static_cast<char>(START_SIGNAL);
	dataBuffer[1] = static_cast<char>(rms & 0xFF); // LSB first
	dataBuffer[2] = static_cast<char>((rms >> 8) & 0xFF); //MSB second 
	dataBuffer[3] = static_cast<char>(peakNRJ);
	dataBuffer[4] = static_cast<char>(zcr & 0xFF);
	dataBuffer[5] = static_cast<char>((zcr >> 8) & 0xFF);
	dataBuffer[6] = static_cast<char>(specCentroid & 0xFF);
	dataBuffer[7] = static_cast<char>((specCentroid >> 8) & 0xFF);
	dataBuffer[8] = static_cast<char>(specCrest & 0xFF);
	dataBuffer[9] = static_cast<char>((specCrest >> 8) & 0xFF);
	dataBuffer[10] = static_cast<char>(specFlatness);
	dataBuffer[11] = static_cast<char>(rollOff);
	dataBuffer[12] = static_cast<char>(specKurtosis & 0xFF);
	dataBuffer[13] = static_cast<char>((specKurtosis >> 8) & 0xFF);
	dataBuffer[14] = static_cast<char>(nrjDiff & 0xFF);
	dataBuffer[15] = static_cast<char>((nrjDiff >> 8) & 0xFF);
	dataBuffer[16] = static_cast<char>(specDiff & 0xFF);
	dataBuffer[17] = static_cast<char>((specDiff >> 8) & 0xFF);
	dataBuffer[18] = static_cast<char>(hfContent & 0xFF);
	dataBuffer[19] = static_cast<char>((hfContent >> 8) & 0xFF);
	dataBuffer[20] = static_cast<char>(pitch & 0xFF);
	dataBuffer[21] = static_cast<char>((pitch >> 8) & 0xFF);
	dataBuffer[22] = static_cast<char>(END_SIGNAL);
	
	// Clear data in the serial buffer
	serial_m.flush(true, true); 
	
	// Write datas to the buffer
	serial_m.writeBytes(&dataBuffer[0], SERIAL_BUFFER_LENGTH);
}

void ofApp::guiValueUpdate()
{
    // Empirical min & max value
    valuesToDisplay_m[0].set("RMS", soundAnalyzer_m->getRMS(), 0, 1);
    valuesToDisplay_m[1].set("Peak Energy", soundAnalyzer_m->getPeakEnergy(), 0, 1);
    valuesToDisplay_m[2].set("ZCR", soundAnalyzer_m->getZeroCrossingRate(), 0, 100);
    valuesToDisplay_m[3].set("Spec. Centroid", soundAnalyzer_m->getSpectralCentroid(), 0, 100);
    valuesToDisplay_m[4].set("Spec. Crest", soundAnalyzer_m->getSpectralCrest(), 0, 300);
    valuesToDisplay_m[5].set("Spec. Flatness", soundAnalyzer_m->getSpectralFlatness(), 0.98, 1);
    valuesToDisplay_m[6].set("Spec. RollOff", soundAnalyzer_m->getSpectralRollOff(), 0, 0.1);
    valuesToDisplay_m[7].set("Spec. Kurtosis", soundAnalyzer_m->getSpectralKurtosis(), 0, 200);
    valuesToDisplay_m[8].set("Spec. Diff.", soundAnalyzer_m->getSpectralDiff(), 0, 20);
    valuesToDisplay_m[9].set("Energy Diff.", soundAnalyzer_m->getEnergyDiff(), 0, 0.1);
    valuesToDisplay_m[10].set("HF Content", soundAnalyzer_m->getHFContent(), 0, 200);
    valuesToDisplay_m[11].set("Main Pitch", soundAnalyzer_m->getPitch(), 0, 600); 
}

void ofApp::setupSound()
{
	//ofSoundStreamSetup(NB_OUTPUTS, NB_INPUTS, SAMPLE_RATE, BUFFER_SIZE, NB_BUFFER);
						 
	soundAnalyzer_m = new Erreur404Sound::SoundAnalyzer();
	cout << "\n\n--------------Sound Stream--------------\n" << endl;
	//ofSoundStreamListDevices();
	soundstream_m.printDeviceList();
	cout << "\n\n----------------------------------------\n" << endl;
	soundstream_m.setDeviceID(5);
	soundstream_m.setup(this, NB_OUTPUTS, NB_INPUTS, SAMPLE_RATE, BUFFER_SIZE, NB_BUFFER);
}

void ofApp::setupSerialCom()
{
	cout << "\n\n--------------Serial Communication--------------\n" << endl;
	cout << "Baud rate: "<< BAUD_RATE <<endl;
	cout << "Serial devices:\n"<< endl;
	serial_m.listDevices();
	
	// TODO try catch if port is not opened
	
	// ------------- Detect the port where the PL2303 is plugged in
	#ifdef OS_UNIX
	// Call the script that detect the port and write in a file this information
	system("../script/script.sh");
	
	#elif defined(OS_Windows)
	// The same on window
	// TODO
	#else
	// Error, can't detect the OS
	#endif
	
	// NOTE: to know serial connection in linux, check dmesg | grep tty
	// How to automatically detect this ? FIXME add a script to look at it and retrieve the good port
	serial_m.setup("/dev/ttyUSB0", BAUD_RATE);
}

void ofApp::debugTerm()
{
}

void ofApp::soundSpectro()
{
	int x, y, width, height;
	
	ofSetColor(255,255,255);
	for(int i = 0; i < 256; i++)
	{
		width = 1024 / 256;
		height = ofMap(soundAnalyzer_m->getMagnitudeSpectrum()[i], 0, 3, 0, 768);
		x = width*(i+1);
		y = 1024 - height;
		
		ofDrawRectangle(x, y, width, height);
	}
}




